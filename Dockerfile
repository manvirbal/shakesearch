FROM golang:1.15-alpine

WORKDIR /shakesearch

COPY *.go .
COPY go.* .
COPY *.txt .
COPY shakesearch.bleve/ ./shakesearch.bleve/

COPY frontend/build ./frontend/build
EXPOSE 3001
RUN go build

CMD ./shakesearch 
