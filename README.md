# ShakeSearch

You can see a live version of the app at https://shakesearch1.herokuapp.com/ Try searching for "Hamlet" to display
a set of results.

## Your Mission

Improve the search backend. Think about the problem from the user's perspective
and prioritize your changes according to what you think is most useful.

To submit your solution, fork this repository and send us a link to your fork
after pushing your changes. The project includes a Heroku Procfile and, in its
current state, can be deployed easily on Heroku's free tier.

If you are stronger on the front-end, complete the react-prompt.md in this
folder.

## Installation

#### Locally

> run `npm run build` in the frontend folder

> run `docker build -t "shakesearch" .` in the parent folder

> run `docker run -p 3001:3001 shakesearch` in the parent folder

#### Heroku Cli

After doing the login

> heroku container:push web

> heroku container:release web

## Backend

The search in the sample files for this was very minimal.
I thought about a few ways to make this better the first thing that came to mind
was to normalize the suffix array by making all characters lowercase. Later, I
scraped this idea thinking it was too easy to get an interview. I turned to
elasticsearch which uses Lucence in the backend. ElasticSearch is amazing and I
wanted to use it but then I came across Bleve.

#### Bleve

Bleve is a super simple and powerful library that allows you to do full-text search
and indexing. Bleve is an open source project started by Engineers at CouchBase.
The head of the project is Marty Schoch and I definitely recommend watching his
videos on Bleve.

I thought Bleve would definitely improve the search capabilities of the application.

I changed the entire search service by shifting away from Suffix array and embracing
Bleve. With the power of Bleve, the user can fuzzy search, case agnostic search,
regex search, and phrase search. Bleve does a few more other things like showing
statistics on each query and how close each result was to the query. I started
out by using batches to index all the lines in the `completeworks.txt` file with
a batch size of 25 lines. The indexing is super fast for all the functionality
you get. It finishes indexing completely within 16s with over 6,000 indices.
Additionally, Bleve allowed me to specify an analyzer which would help in
removing, expanding, and stemming words. I went with the simple analyzer testing
all the analyzers out and even making a custom one.

At the current moment when you search for
anything on the site it returns the first 10 matches. I think another
improvement I can make in the future is to add pagination when you scroll on the
results.

Overall, I completely got rid of the old approach of using a suffix array to search,
and replaced that with a really nice library that is made for searching in text.

## Frontend

I decided to do the frontend of the challenge because it looked fun and because
I also want to apply for the Full Stack position.

I first started out by creating the search input tag and customizing that. The
hardest part was getting the everything aligned and working in a row. The animations
for the arrow button and other images was pretty easy to do since css allows you
to do that pretty easily. After getting that working, I started working on the
shakespeare image which is positioned in the right bottom corner. I think the
image could be improved because when you shrink the window the image gets
distorted which doesn't look great. Lastly, I had to get the paper with the
results of the query on it. I achieved that paper look by using a div and using
a `10px` border-radius which curves the corners. For getting the results, I used
fetch and added the results using the innerhtml since Bleve escapes the characters.
Bleve also allows you to highlight the words that match the query using
mark tags which is why those words are yellow in the result.

I tried to make sure my website matched what was in the demo gif in the sample
repo. I think the website can be improved by adding a section that allows you to
specify which person said the word(s) you are querying for. Another improvement
is to add pagination.
