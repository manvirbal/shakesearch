package main

import (
	"github.com/blevesearch/bleve"
	"github.com/blevesearch/bleve/mapping"
)

func buildIndex() (mapping.IndexMapping, error) {
	mapping := bleve.NewTextFieldMapping()

	shake := bleve.NewDocumentMapping()
	//completeworks
	shake.AddFieldMappingsAt("completeworks", mapping)
	indexMapping := bleve.NewIndexMapping()
	indexMapping.AddDocumentMapping("shakespeare", shake)
	indexMapping.TypeField = "type"
	indexMapping.DefaultAnalyzer = "simple"

	return indexMapping, nil

}
