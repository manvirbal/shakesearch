package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"

	bleve "github.com/blevesearch/bleve"
	_ "github.com/blevesearch/bleve/config"
)

func main() {

	indexMapping, _ := buildIndex()
	shakeIndex, err := bleve.New("shakesearch.bleve", indexMapping)
	if err == nil {
		go func() {
			err = indexShakeSpeare("completeworks.txt", shakeIndex)
			if err != nil {
				log.Fatalf("Error in indexShakeSpeare: %v\n", err)
			}
			shakeIndex.Close()
		}()
	} else {
		log.Println("Index already exists...")
	}

	fs := http.FileServer(http.Dir("./frontend/build"))
	http.Handle("/", fs)

	http.HandleFunc("/search", handleSearch())

	port := os.Getenv("PORT")
	if port == "" {
		port = "3001"
	}

	fmt.Printf("Listening on port %s...", port)
	err = http.ListenAndServe(fmt.Sprintf(":%s", port), nil)
	if err != nil {
		log.Fatal(err)
	}
}

type shakeSpeare struct {
	CompleteWorks string `json:"completeworks"`
}

func handleSearch() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		query, ok := r.URL.Query()["q"]
		if !ok || len(query[0]) < 1 {
			w.WriteHeader(http.StatusBadRequest)
			w.Write([]byte("missing search query in URL params"))
			return
		}
		results := search(query[0])
		buf := &bytes.Buffer{}
		enc := json.NewEncoder(buf)
		err := enc.Encode(results)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte("encoding failure"))
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(buf.Bytes())
	}
}

func indexShakeSpeare(filename string, index bleve.Index) error {
	startTime := time.Now()
	dat, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	lines := strings.Split(string(dat), "\n")
	totalLines := len(lines)
	batchSize := 25
	batch := index.NewBatch()
	numOfBatches := 0

	for start := 0; start < totalLines; start += batchSize {
		end := start + batchSize
		if end > totalLines {
			end = totalLines - 1
		}

		shake := shakeSpeare{strings.Join(lines[start:end], "\n")}
		batch.Index(fmt.Sprintf("%d-%d", start, end), shake)
		if batch.Size() > 100 {
			err := index.Batch(batch)
			if err != nil {
				return err
			}
			numOfBatches += batch.Size()
			batch = index.NewBatch()
		}
	}

	if batch.Size() > 0 {
		err := index.Batch(batch)
		numOfBatches += batch.Size()
		if err != nil {
			return err
		}
	}

	timeSpent := time.Since(startTime)
	log.Printf("Finished indexing %v batches in %.2f\n", numOfBatches, timeSpent.Seconds())

	return nil
}

func search(query string) []string {
	res := []string{}
	if bindex, err := bleve.Open("shakesearch.bleve"); err == nil {

		fuzzQuery := bleve.NewFuzzyQuery(query)
		bQuery := bleve.NewQueryStringQuery(query)
		regQuery := bleve.NewRegexpQuery("[a-zA-Z0-9_]*" + query + "[a-zA-Z0-9_]*")
		que := bleve.NewDisjunctionQuery()
		que.AddQuery(fuzzQuery)
		que.AddQuery(bQuery)
		que.AddQuery(regQuery)

		searchReq := bleve.NewSearchRequest(que)
		searchReq.Highlight = bleve.NewHighlightWithStyle("html")
		searchRes, err := bindex.Search(searchReq)
		bindex.Close()
		if err != nil {
			log.Println("Log search err: ", err)
			return res
		}

		for _, hits := range searchRes.Hits {
			for _, fragments := range hits.Fragments {
				res = append(res, fragments...)
			}
		}
		return res

	}
	return res
}
