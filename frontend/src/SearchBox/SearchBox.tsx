import React, { useState } from 'react';
import './SearchBox.css';
import rightarrow from './../assets/rightarrow.svg'
import underline from './../assets/lineunder.svg'
import shakespeare from './../assets/shakespeare.png'
import mag from './../assets/searchIcon.svg'

export default function SearchBox(this: any) {
    const [showSubmitButton, setShowSubmitButton] = useState(false);
    const [searchVal, setSearchVal] = useState("")
    const [clickedSubmit, setClickedSubmit] = useState(false);

    function PushedSubmit() {
        setClickedSubmit(true);
        fetch(`/search?q=${searchVal}`)
        .then(response => response.json())
        .then(data => {
            let rows = "";
            for (let result of data){
                rows += "<br><br><br>" + result;
            }
            const resDiv = document.querySelector('.Results');
            if (resDiv) {
                resDiv.innerHTML = rows;
            }
        });
    }

    function InputFocus() {
        setShowSubmitButton(true);
    }
    function OutOfFocus() {
        if (searchVal === "") {
            setShowSubmitButton(false);
            setClickedSubmit(false);
        }
    }
    function ClickingToSearchAgain(e: any) {
        if (clickedSubmit) {
            setShowSubmitButton(false);
            setClickedSubmit(false);
            setSearchVal("")
            e.target.blur();
        }
    }


    return (
        <div  className="Search">
            <div 
                className="LeftPane"
                style={{width: clickedSubmit ? '45%' : '60%'}}
            >
                <div className="SearchDiv">
                    <div className="ShakeDiv">
                        <img 
                            className="Magnifer" 
                            src={mag}
                            alt={"Search Magnifer for searching"}
                        />
                        <input
                            type="text"
                            className="ShakeSearch"
                            onFocus={InputFocus}
                            value= {searchVal}
                            onChange={(e) => setSearchVal(e.target.value)}
                            onBlur={OutOfFocus}
                            onClick={(e) => ClickingToSearchAgain(e)}
                            placeholder={"What art thee looking f'r?"}
                        />
                        <div className="ShakeSubmit"
                            style={{display: showSubmitButton && !clickedSubmit ? 'block' : 'none', 
                            animation: showSubmitButton ? 'SubmitButtonMoveRight 0.5s forwards' : ''}}
                        >
                            <img 
                                className="SubmitImage" 
                                src={rightarrow} 
                                alt={"Arrow for submitting search value"}
                                onClick={PushedSubmit} 
                            />
                        </div>
                    </div>
                    <div 
                        className="Underline"
                        style={{animation: clickedSubmit ? 'LineMoveOut 0.5s forwards': 'LineMoveIn 0.5s forwards'}}
                    >
                        <img 
                            className= "Whiteline" 
                            src={underline}
                            alt={"A white line for decorative purposes"}
                        />
                    </div>
                </div>
            </div>
            <div 
                className="RightPane"
                style={{width: clickedSubmit ? '55%' : '40%'}}
            >
                <div 
                    className="Paper"
                    style= {{display: clickedSubmit ? 'flex' : 'none',
                    animation: clickedSubmit ? 'PaperMoveIn .36s forwards' : ''}}
                >
                    <div 
                        className="Results"
                    />
                </div>
                <img 
                    className= "Shakespeare" 
                    style={{animation: clickedSubmit ? 'SSMoveOut 0.5s forwards': 'SSMoveIn 0.5s forwards'}}
                    src={shakespeare}
                    alt={"Shakespeare chilling for decorative purposes"}
                />
            </div>
        </div>
    );
}